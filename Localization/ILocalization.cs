namespace MChat.BackEnd.Localization
{
    public interface ILocalization
    {
        string UserInvited { get; }
        string UserKicked { get; }
        string MessagePinned { get; }
        string MessageUnpinned { get; }
        string ConversationPhotoUpdated { get; }
        string ConversationPhotoRemove { get; }
        string ConversationCreated { get; }
        string ConversationTitleUpdated { get; }
        string Me { get; }
    }
}