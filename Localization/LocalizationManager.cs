using System;

namespace MChat.BackEnd.Localization
{
    public class LocalizationManager
    {
        public ILocalization Localization { get; private set; } = new RuRuLocalization();
        public event EventHandler LocalizationWasChanged;
        public static LocalizationManager Instance
        {

            get
            {
                if (_instance is null)
                {
                    lock (_syncObject)
                    {
                        if (_instance is null)
                        {
                            _instance = new LocalizationManager();
                        }
                    }
                }

                return _instance;
            }
        }
        private static object _syncObject = new();
        private static LocalizationManager _instance;

        public void ChangeLocalization(ILocalization newLocalization)
        {
            Localization = newLocalization;
            OnLocalizationWasChanged();
        }

        protected virtual void OnLocalizationWasChanged()
        {
            EventHandler eventHandler = LocalizationWasChanged;
            eventHandler?.Invoke(this, new EventArgs());
        }
    }
}