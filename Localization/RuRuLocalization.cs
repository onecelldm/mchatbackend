namespace MChat.BackEnd.Localization
{
    public class RuRuLocalization : ILocalization
    {
        public string UserInvited { get; } = "Пользователь '{0}' присоеденился";
        public string UserKicked { get; } = "Пользователь '{0}' исключен";
        public string MessagePinned { get; } = "Сообщение закреплено";
        public string MessageUnpinned { get; } = "Сообщение откреплено";
        public string ConversationPhotoUpdated { get; } = "Фотография беседы обновлена";
        public string ConversationPhotoRemove { get; } = "Фотография беседы удалена";
        public string ConversationCreated { get; } = "Беседа создана";
        public string ConversationTitleUpdated { get; } = "Название беседы обновлено на '{0}'";
        public string Me { get; } = "Я: ";
    }
}