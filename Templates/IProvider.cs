using System;
using System.Collections.ObjectModel;
using MChat.BackEnd.DataModels;

namespace MChat.BackEnd.Templates
{
    public interface IProvider
    {
        event EventHandler<NewMessageReceivedEventArgs> NewMessageReceived;
        event EventHandler<UserIsTypingEventArgs> UserIsTyping;
        event EventHandler<UsersAreTypingEventArgs> UsersAreTyping;
        event EventHandler<UsersAreRecordingAVoiceMessageEventArgs> UsersAreRecordingAVoiceMessage;
        event EventHandler<ConversationTitleWasUpdatedEventArgs> ConversationTitleWasUpdated;
        event EventHandler<ConversationPhotoWasUpdatedEventArgs> ConversationPhotoWasUpdated;
        AccountModel Account { get; set; }

        void Authenticate(string login, string password, string twoFacCode = "");
        void Authenticate(AccountModel account);

        void DownloadAvatar();
        void SendMessage(MessageModel message);
        void StartListener();
        void StopListener();
        ulong GetDialogueUnreadMessagesCount();
        Collection<DialogueShort> GetDialogueList(ulong offset = 0, ulong count = 50);
        Collection<MessageModel> GetDialogueMessageHistory(string dialogueId, long offset = 0);
    }
}