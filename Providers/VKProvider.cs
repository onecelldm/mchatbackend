using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using MChat.BackEnd.DataModels;
using MChat.BackEnd.Exceptions;
using MChat.BackEnd.Localization;
using MChat.BackEnd.Templates;
using MChat.BackEnd.Utils;
using MChat.BackEnd.Utils.VK;
using Newtonsoft.Json.Linq;

using VkNet;
using VkNet.Enums.Filters;
using VkNet.Exception;
using VkNet.Model;
using VkNet.Utils;

namespace MChat.BackEnd.Providers
{
    public class VKProvider : IProvider
    {

        public event EventHandler<NewMessageReceivedEventArgs> NewMessageReceived
        {
            add => _vkLongPoll.LPNewMessageReceived += value;
            remove => _vkLongPoll.LPNewMessageReceived -= value;
        }
        public event EventHandler<UserIsTypingEventArgs> UserIsTyping
        {
            add => _vkLongPoll.LPUserIsTyping += value;
            remove =>_vkLongPoll.LPUserIsTyping -= value;
            
        }
        public event EventHandler<UsersAreTypingEventArgs> UsersAreTyping{
            add => _vkLongPoll.LPUsersAreTyping += value;
            remove => _vkLongPoll.LPUsersAreTyping -= value;
        }

        public event EventHandler<UsersAreRecordingAVoiceMessageEventArgs> UsersAreRecordingAVoiceMessage
        {
            add => _vkLongPoll.LPUsersAreRecordingAVoiceMessage += value;
            remove => _vkLongPoll.LPUsersAreRecordingAVoiceMessage -= value;
        }

        public event EventHandler<ConversationTitleWasUpdatedEventArgs> ConversationTitleWasUpdated
        {
            add => VKUtils.VKUConversationTitleWasUpdated += value;
            remove => VKUtils.VKUConversationTitleWasUpdated -= value;
        }

        public event EventHandler<ConversationPhotoWasUpdatedEventArgs> ConversationPhotoWasUpdated
        {
            add => VKUtils.VKUConversationPhotoWasUpdated += value;
            remove => VKUtils.VKUConversationPhotoWasUpdated -= value;
        }

        public const string ProviderId = "whtntr.vkprovider";
        public VkFactory Factory;
        public AccountModel Account { get; set; } = new AccountModel();
        /// <summary>Ник авторизовавшегося пользователя</summary>
        private VkApi _vkApi;
        private VKLongPoll _vkLongPoll;

        public VkApi VkApi { get => _vkApi; }

        public VKProvider()
        {
            _vkApi = new VkApi();
            Account.ProviderId = ProviderId;
            Factory = new VkFactory(this);
        }

        public void Authenticate(string login, string password, string twoFacCode)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new PasswordRequired();
            }

            string AuthUrl = $"https://oauth.vk.com/token?username={login}&password={password}" +
                          "&grant_type=password&2fa_supported=1&client_secret=L3yBidmMBtFRKO9hPCgF" +
                          "&client_id=6121396";

            JObject ResObject;

            if (twoFacCode != "")
            {
                ResObject = VkLoginRequest($"{AuthUrl}&code={twoFacCode}").GetAwaiter().GetResult();
            }
            else ResObject = VkLoginRequest(AuthUrl).GetAwaiter().GetResult();


            if (ResObject.ContainsKey("access_token"))
            {
                Account.Token = ResObject["access_token"].ToObject<string>();
                Account.Login = ResObject["user_id"].ToObject<long>().ToString();
                _vkApi.Authorize(new ApiAuthParams()
                {
                    AccessToken = Account.Token,
                    UserId = long.Parse(Account.Login)
                });
                AfterAuth();
            }
            else
            {
                throw new UnknownAuthError();
            }

        }

        public void Authenticate(AccountModel account)
        {
            Account = account;
            _vkApi.Authorize(new ApiAuthParams()
            {
                AccessToken = Account.Token,
                UserId = long.Parse(Account.Login)
            });
            AfterAuth();
        }

        public void DownloadAvatar()
        {
            User user = _vkApi.Users.Get(new Collection<long>() { long.Parse(Account.Login) },
                                         ProfileFields.Photo50 | ProfileFields.PhotoMaxOrig)[0];

            Account.AvatarUrl = user.Photo50.ToString();
            Account.AvatarUrl = user.PhotoMaxOrig is not null
                                ? user.PhotoMaxOrig.ToString() : Account.AvatarUrl;
        }

        public void StartListener()
        {
            _vkLongPoll.Work = true;
            _vkLongPoll.Update();
        }

        public void StopListener()
        {
            _vkLongPoll.Work = false;
        }

        public void SendMessage(MessageModel message)
        {
            long peerId = 0;

            switch (message.DialogueType)
            {
                case DialogueType.Conversation:
                    peerId = long.Parse(message.DialogueId) + 2000000000;
                    break;
                case DialogueType.Group:
                    peerId = long.Parse(message.DialogueId) * -1;
                    break;
                case DialogueType.Private:
                    peerId = long.Parse(message.DialogueId);
                    break;
            }

            _vkApi.Messages.Send(new VkNet.Model.RequestParams.MessagesSendParams
            {
                PeerId = peerId,
                Message = message.Body,
                RandomId = MChat.BackEnd.Utils.GlobalInstances.Random.Next()
            });
        }

        Collection<DialogueShort> IProvider.GetDialogueList(ulong offset, ulong count)
        {
            Collection<DialogueShort> dialogueCollection = new();
            GetConversationsResult result = _vkApi.Messages.GetConversations(new VkNet.Model.RequestParams.GetConversationsParams
            {
                Extended = true,
                Count = count,
                Offset = offset,
            });

            foreach (ConversationAndLastMessage convAndLastMsg in result.Items)
            {
                Conversation conversation = convAndLastMsg.Conversation;

                if (conversation.Peer.Type == VkNet.Enums.SafetyEnums.ConversationPeerType.Chat)
                {
                    dialogueCollection.Add(Factory.CreateConversationDialogue(conversation,
                                                                    convAndLastMsg.LastMessage,
                                                                    result.Profiles,
                                                                    result.Groups));
                }
                else if (conversation.Peer.Type == VkNet.Enums.SafetyEnums.ConversationPeerType.User)
                {
                    dialogueCollection.Add(Factory.CreatePrivateDialogue(conversation,
                                                               convAndLastMsg.LastMessage,
                                                               result.Profiles,
                                                               result.Groups));
                }
                else if (conversation.Peer.Type == VkNet.Enums.SafetyEnums.ConversationPeerType.Group)
                {
                    dialogueCollection.Add(Factory.CreateGroupDialogue(conversation,
                                                             convAndLastMsg.LastMessage,
                                                             result.Profiles,
                                                             result.Groups));
                }
                dialogueCollection.Last().LastMessageTs =
                    ((DateTimeOffset)convAndLastMsg.LastMessage.Date).ToUnixTimeSeconds();

                if (convAndLastMsg.LastMessage.Attachments.Count > 0 || convAndLastMsg.LastMessage.ForwardedMessages.Count > 0)
                    dialogueCollection.Last().IsAttachment = true;

                dialogueCollection.Last().Login = Account.Login;
            }

            return dialogueCollection;
        }

        // ? dialogueID в данном случае это peerId
        Collection<MessageModel> IProvider.GetDialogueMessageHistory(string dialogueId, long offset)
        {
            Collection<MessageModel> messageHistory = new ();
            var history = _vkApi.Messages.GetHistory(new()
            {
                PeerId = long.Parse(dialogueId),
                Count = 200,
                Offset = offset,
                Extended = true
            });

            foreach (Message message in history.Messages)
            {
                // Collection<User> profiles = history.Users.SafeToCollection();
                // Collection<Group> groups = history.Groups.SafeToCollection();
                // я оставил это на случай если понадобится тащить отсюда никнейм

                messageHistory.Add(Factory.CreateMessageModel(message));
            }

            return messageHistory;
        }

        public ulong GetDialogueUnreadMessagesCount()
        {
            ulong count = 0;
            GetConversationsResult result = _vkApi.Messages.GetConversations(new VkNet.Model.RequestParams.GetConversationsParams()
            {
                Count = 200,
                Filter = VkNet.Enums.SafetyEnums.GetConversationFilter.Unread
            });
            foreach (var conversation in result.Items)
                count += (ulong)conversation.Conversation.UnreadCount;

            return count;

        }
        private void AfterAuth()
        {
            Account.Nickname = VKUtils.GetNicknameFromApi(_vkApi);
            Console.WriteLine($"{Account.Nickname}: Login succsessfull");
            _vkLongPoll = new VKLongPoll(this, Account.Nickname);
        }

       
        private async Task<JObject> VkLoginRequest(string url)
        {
            MChat.BackEnd.Utils.GlobalInstances.HttpClient.DefaultRequestHeaders.Add("Connection", "Keep-Alive");
            MChat.BackEnd.Utils.GlobalInstances.HttpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var res = MChat.BackEnd.Utils.GlobalInstances.HttpClient.GetAsync(url).Result;
            MChat.BackEnd.Utils.GlobalInstances.HttpClient.DefaultRequestHeaders.Clear();

            string awaitData = await res.Content.ReadAsStringAsync();
            JObject obj = JObject.Parse(awaitData);

            if (obj.ContainsKey("error") is false)
            {
                return obj;
            }

            switch (obj["error"].ToObject<string>())
            {
                case "need_captcha":
                    throw new CaptchaNeededException(new VkError()
                    {
                        ErrorCode = VkErrorCode.CaptchaNeeded,
                        CaptchaSid = obj["captcha_sid"].ToObject<ulong>(),
                        CaptchaImg = obj["captcha_img"].ToObject<Uri>(),
                    });
                case "need_validation":
                    throw new TwoFacRequired();
                case "invalid_client":
                    throw new IncorrectLoginData();
                default:
                    throw new UnknownAuthError(obj["error_description"].ToObject<string>());
            }
        }

    }
}