namespace MChat.BackEnd.DataModels
{
    public class DialogueShort
    {
        // я не знал как еще обозвать этот объект, по этому он будет коротким.
        // *шутка про хуй вырезана цензурой*

        public string Title { get; set; }
        /// <summary>Самая маленькая по разрешению из доступных аватарка</summary>
        public string AvatarUrl { get; set; }
        /// <summary>Самая большая по разрешению из доступных аватарок</summary>
        public string FullAvatarUrl { get; set; }
        public long UnreadCount { get; set; }
        public MessageModel LastMessage { get; set; }
        public long LastMessageTs { get; set; }
        public bool IsAttachment { get; set; }
        public string Login { get; set; }
        public DialogueType DialogueType { get; set; }
        public string Id { get; set; }
    }
}