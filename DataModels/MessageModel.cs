using System;
using System.Collections.ObjectModel;
using MChat.BackEnd.DataModels.Attachments;
using MChat.BackEnd.Utils.VK;
using VkNet.Enums;
using VkNet.Model;

namespace MChat.BackEnd.DataModels
{
    public class MessageModel
    {
        public string Body { get; set; } = "";
        public string SendersNickname { get; set; } = "";
        public string SendersId { get; set; } = "";
        public string Id { get; set; } = "";
        public string DialogueId { get; set; } = "";
        public DialogueType DialogueType { get; set; }
        public Collection<IAttachmentModel> Attachments { get; set; } = new ();
        public Collection<MessageModel> FwdMessages { get; set; } = new ();
        public bool Readed { get; set; }
        public bool Service { get; set; }
        public long Ts { get; set; }

        public MessageModel() {}
        public MessageModel(Message message)
        {
            long peerId = message.PeerId ?? 0;
            Body = message.Text;
            ScreenMessage();
            DialogueType = VKUtils.DetermineDialogueTypeFromPeerId(peerId);
            Id = message.Id.ToString();
            DialogueId = VKUtils.PeerIdToId(peerId, DialogueType).ToString();
            SendersId = message.FromId.ToString();
            Readed = message.ReadState == MessageReadState.Readed ? true : false;
            Ts = ((DateTimeOffset)message.Date).ToUnixTimeSeconds();
        }

        /// <summary>по дефолту сообщения с вк приходят в экранированном виде, типа вместо скобок будет какая-то хрень. Тут я это и исправляю</summary>
        public void ScreenMessage()
        {
            Body = Body.Replace("&lt;", "<");
            Body = Body.Replace("&gt;", ">");
            Body = Body.Replace("&quot;", "\"");
            Body = Body.Replace("&amp;", "&");
        }
    }
}