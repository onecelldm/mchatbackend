using MChat.BackEnd.Templates;

namespace MChat.BackEnd.DataModels
{
    public class ConversationTitleWasUpdatedEventArgs : System.EventArgs
    {
        public string DialogueId { get; set; }
        public string NewTitle { get; set; }
        public IProvider Provider { get; set; }
    }
}