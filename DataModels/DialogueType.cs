namespace MChat.BackEnd.DataModels
{
    public enum DialogueType
    {
        Private,
        Conversation,
        Group
    }
}