using System;
using System.Collections.ObjectModel;
using MChat.BackEnd.Templates;

namespace MChat.BackEnd.DataModels
{
    public class UsersAreTypingEventArgs : EventArgs
    {
        public Collection<ulong> UsersId { get; set; }
        public long PeerId { get; set; }
        public long DialogueId { get; set; }
        public DialogueType DialogueType { get; set; }
        /// <summary>кол-во печатающих сейчас</summary>
        public int TotalCount { get; set; }
        public IProvider Provider { get; set; }
    }
}