namespace MChat.BackEnd.DataModels
{
    public class UserModel
    {
        public string Nickname { get; set; }
        public string AvatarUrl { get; set; }
        public string FullAvatarUrl { get; set; }
        public string Id { get; set; }
    }
}