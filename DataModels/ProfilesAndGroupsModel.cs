using System.Collections.ObjectModel;
using VkNet.Model;

namespace MChat.BackEnd.DataModels
{
    public class ProfilesAndGroupsModel
    {
        public Collection<User> Users { get; set; } = new Collection<User>();
        public Collection<Group> Groups { get; set; } = new Collection<Group>();
    }
}