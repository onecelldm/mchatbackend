using Newtonsoft.Json;

namespace MChat.BackEnd.DataModels
{
    public class AccountModel
    {
        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("provider_id")]
        public string ProviderId { get; set; }

        /// <summary>Самая маленькая по разрешению из доступных аватарка</summary>
        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        /// <summary>Самая большая по разрешению из доступных аватарок</summary>
        [JsonProperty("full_avatar_url")]
        public string FullAvatarUrl { get; set; }
    }
}