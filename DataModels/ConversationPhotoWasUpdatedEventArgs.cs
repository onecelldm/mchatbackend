using System.Collections.ObjectModel;
using MChat.BackEnd.DataModels.Attachments;
using MChat.BackEnd.Templates;

namespace MChat.BackEnd.DataModels
{
    public class ConversationPhotoWasUpdatedEventArgs : System.EventArgs
    {
        public string Photo50 { get; set; }
        public string Photo100 { get; set; }
        public string Photo200 { get; set; }
        public IProvider Provider { get; set; }
    }
}