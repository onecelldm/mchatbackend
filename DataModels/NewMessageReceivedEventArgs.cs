using System;
using MChat.BackEnd.Templates;

namespace MChat.BackEnd.DataModels
{
    public class NewMessageReceivedEventArgs : EventArgs
    {
        public MessageModel Message { get; set; }
        public IProvider Provider { get; set; }
    }
}