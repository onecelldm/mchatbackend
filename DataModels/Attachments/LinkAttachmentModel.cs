namespace MChat.BackEnd.DataModels.Attachments
{
    public class LinkAttachmentModel : IAttachmentModel
    {
        public string Caption { get; set; }
        public string Description { get; set; }
        /// <summary>ссылка на изображения превью сайта</summary>
        public string PreviewUrl { get; set; }
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }

    }
}