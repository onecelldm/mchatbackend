
using System.Collections.ObjectModel;

namespace MChat.BackEnd.DataModels.Attachments
{
    public class AudioAttachmentModel : IAttachmentModel
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }
    }
}