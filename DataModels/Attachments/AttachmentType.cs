namespace MChat.BackEnd.DataModels.Attachments
{
    public enum AttachmentType
    {
        Photo,
        VoiceMessage,
        Audio,
        Video,
        Document,
        Link,
        Sticker
    }
}