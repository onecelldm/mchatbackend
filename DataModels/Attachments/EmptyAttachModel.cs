using MChat.BackEnd.DataModels.Attachments;

namespace MChat.BackEnd.Utils.VK
{
    internal class EmptyAttachModel : IAttachmentModel
    {
        public string Id { get; set; }
        public string AccessKey{ get; set; }
        public string Owner{ get; set; }
        public string Url{ get; set; }
        public string OwnerId { get; set; }
    }
}