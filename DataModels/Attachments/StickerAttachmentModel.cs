using System.Collections.ObjectModel;

namespace MChat.BackEnd.DataModels.Attachments
{
    public class StickerAttachmentModel : IAttachmentModel
    {
        /// <summary>айди набора</summary>
        public string ProductId { get; set; }
        public Collection<PhotoAttachmentModel> Sizes { get; set; }
        public string AnimationUrl { get; set; }
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        public long Timestamp { get; set; }
        public AttachmentType Type { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }
    }
}