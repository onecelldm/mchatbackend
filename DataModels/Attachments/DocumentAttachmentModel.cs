namespace MChat.BackEnd.DataModels.Attachments
{
    public class DocumentAttachmentModel : IAttachmentModel
    {
        public string Title { get; set; }
        /// <summary>размер файла в байттах</summary>
        public long Size { get; set; }
        public string Extension { get; set; }
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }
    }
}