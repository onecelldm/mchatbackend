using System.Collections.ObjectModel;

namespace MChat.BackEnd.DataModels.Attachments
{
    public class PhotoAttachmentModel : IAttachmentModel
    {
        public ulong Width { get; set; }
        public ulong Height { get; set; }
        public Collection<PhotoAttachmentModel> Sizes;
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }
    }
}