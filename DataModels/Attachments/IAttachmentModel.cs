namespace MChat.BackEnd.DataModels.Attachments
{
    public interface IAttachmentModel
    {
        string Id { get; set; }
        string OwnerId { get; set; }
        string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        string AccessKey { get; set; }
    }
}