namespace MChat.BackEnd.DataModels.Attachments
{
    public class VideoAttachmentModel : IAttachmentModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public VideoThumbnailModel Thumbnail { get; set; }
        public int Duration { get; set; }
        public int ViewsCount { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Id { get; set; }
        public string OwnerId { get; set; }
        /// <summary>прямая ссылка на видео. Но в случае с вк, пока что выдается ссылка на плеер</summary>
        public string Url { get; set; }
        /// <summary>Ключ доступа к контенту (где он используется)</summary>
        public string AccessKey { get; set; }
    }
}