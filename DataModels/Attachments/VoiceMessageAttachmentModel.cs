using System.Collections.ObjectModel;

namespace MChat.BackEnd.DataModels.Attachments
{
    public class VoiceMessageAttachmentModel : IAttachmentModel
    {
        public string Url { get; set; }
        public string OwnerId { get; set; }
        /// <summary>длительность гс в секундах</summary>
        public int Duration { get; set; }
        public Collection<int> Waveform { get; set; }
        public string Id { get; set; }
        public string AccessKey { get; set; }
    }
}