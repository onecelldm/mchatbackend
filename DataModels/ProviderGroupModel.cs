using System.Collections.ObjectModel;

namespace MChat.BackEnd.DataModels
{
    public class ProviderGroupModel
    {
        public string Title { get; set; }
        public string AvatarUrl { get; set; }
        public Collection<ulong> ProvidersId { get; set; }
    }
}