namespace MChat.BackEnd.DataModels
{
    public class VideoThumbnailModel
    {
        public string Url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}