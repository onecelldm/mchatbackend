using System;
using MChat.BackEnd.Templates;

namespace MChat.BackEnd.DataModels
{
    public class UserIsTypingEventArgs : EventArgs
    {
        public ulong UserId { get; set; }
        public long DialogueId { get; set; }
        public DialogueType DialogueType { get; set; }
        public IProvider Provider { get; set; }
    }
}