using System;
using System.Collections.ObjectModel;
using System.IO;
using MChat.BackEnd.DataModels;
using MChat.BackEnd.Templates;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MChat.BackEnd.Utils
{
    public class ProviderManager
    {
        private static object _SyncObj = new();

        private static ProviderManager _Instance;

        public Collection<IProvider> ProviderList { get; set; } = new();

        public static ProviderManager Instance
        {
            get
            {
                if (_Instance is null)
                {
                    lock (_SyncObj)
                    {
                        if (_Instance is null)
                            _Instance = new ProviderManager();
                    }
                }
                return _Instance;

            }
        }

        private ProviderManager()
        {
            ReadFromAccountDataFile();
        }

        public void RegisterAnAccount(string login, string password, string providerId, string twoFaCode = "") =>
            RegisterAnAccount(login, password, Reflection.GetProviderTypeFromId(providerId), twoFaCode);


        public void RegisterAnAccount(string login, string password, Type ProviderType, string twoFaCode = "")
        {
            IProvider provider = GetProviderToAccount(ProviderType);
            provider.Authenticate(login, password, twoFaCode);
            ProviderList.Add(provider);
            WriteToAccountDataFile();
        }

        public IProvider GetProviderToAccount(string providerId) =>
              Reflection.CreateProvider(Reflection.GetProviderTypeFromId(providerId));

        public IProvider GetProviderToAccount(Type providerType) =>
            Reflection.CreateProvider(providerType);

        public void WriteToAccountDataFile()
        {
            Collection<AccountModel> accounts = new();
            foreach (IProvider provider in ProviderList) accounts.Add(provider.Account);
            ConfigManager.Save(accounts);
        }

        public void ReadFromAccountDataFile()
        {
            var accountList = ConfigManager.Read();
            foreach (var account in accountList)
            {
                IProvider provider = GetProviderToAccount(account.ProviderId);
                provider.Authenticate(account);
                ProviderList.Add(provider);
            }
        }

        public void RemoveProvider(IProvider provider) => ProviderList.Remove(provider);
        public void RemoveProvider(int providerIndex) => ProviderList.RemoveAt(providerIndex);
    }
}