﻿using System;
using System.Linq;
using MChat.BackEnd.Exceptions;
using MChat.BackEnd.Templates;

namespace MChat.BackEnd.Utils
{
    public static class Reflection
    {
        public static Type[] GetProvidersTypes() =>
             System.Reflection.Assembly.GetExecutingAssembly().
                GetTypes().Where(t => t.IsClass
                    && t.Namespace == "MChat.BackEnd.Providers"
                    && !t.Attributes.HasFlag(System.Reflection.TypeAttributes.Sealed)).ToArray();

        public static Type GetProviderTypeFromId(string providerId)
        {
            foreach (var type in GetProvidersTypes())
            {

                var propertyValue = GetProviderId(type);

                if (propertyValue == providerId)
                    return type;
            }
            throw new ProviderNotFoundException();
        }

        public static Type GetProviderTypeFromName(string providerName)
        {

            Type type = GetProvidersTypes().FirstOrDefault(x => x.Name.ToLower() == providerName.ToLower());

            return type != null ? type : throw new ProviderNotFoundException();

        }

        public static string GetProviderId(Type type) =>
            (string)type.GetField("ProviderId")?.GetRawConstantValue();


        public static IProvider CreateProvider(Type providerType) =>
            (IProvider)Activator.CreateInstance(providerType);

    }
}
