﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MChat.BackEnd.Utils
{
    public static class ExtendentMethods
    {
        public static Collection<T> SafeToCollection<T>(this IEnumerable<T> enumerable) =>
            enumerable is null ? new Collection<T>() : new Collection<T>(new List<T>(enumerable));

    }
}
