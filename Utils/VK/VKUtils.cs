using System;
using System.Collections.ObjectModel;
using MChat.BackEnd.DataModels;
using MChat.BackEnd.DataModels.Attachments;
using MChat.BackEnd.Localization;
using MChat.BackEnd.Providers;
using Newtonsoft.Json.Linq;
using VkNet;
using VkNet.Abstractions;
using VkNet.Enums;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Utils;

namespace MChat.BackEnd.Utils.VK
{
    class VKUtils
    {
        public static event EventHandler<ConversationTitleWasUpdatedEventArgs> VKUConversationTitleWasUpdated;
        public static event EventHandler<ConversationPhotoWasUpdatedEventArgs> VKUConversationPhotoWasUpdated;

        public static MessageModel ProcessLongPollMessage(JToken lpMessage, VKProvider provider)
        {
            long peerId = (long)lpMessage[3];
            DialogueType dialogueType = DetermineDialogueTypeFromPeerId(peerId);
            string id = PeerIdToId(peerId, dialogueType).ToString();

            MessageModel messageModel = new()
            {
                Body = (string)lpMessage[5],
                Id = (string)lpMessage[1],
                DialogueId = id,
                DialogueType = dialogueType, 
                Ts = (long)lpMessage[4]
            };
            if (lpMessage[6] is not null && lpMessage[6]["attach1"] is not null)
                messageModel.Attachments.Add(new EmptyAttachModel());
            return messageModel;
        }


        private static void GetNicknameAndSendersId(MessageModel message, VKProvider provider)
        {
            string nickname = provider.Account.Nickname;
            VkApi vkApi = provider.VkApi;

            switch (message.DialogueType)
            {
                case DialogueType.Private:
                    if (message.Id == (vkApi.UserId ?? 0).ToString())
                        message.SendersNickname = nickname;
                    User user = GetUserById(message.DialogueId, vkApi);

                    message.SendersId = user.Id.ToString();
                    message.SendersNickname = $"{user.FirstName} {user.LastName}";
                    break;
                case DialogueType.Conversation:
                    if (message.Id == (vkApi.UserId ?? 0).ToString())
                        message.SendersNickname = nickname;

                    var ids2 = new Collection<ulong>() { ulong.Parse(message.Id) };
                    var fields = new Collection<string>() { };

                    VkCollection<Message> messages = vkApi.Messages.GetById(ids2, fields, extended: true);
                    long userId = messages[0].FromId ?? 0;
                    User user2 = vkApi.Users.Get(new Collection<long>() { userId })[0];

                    message.SendersId = user2.Id.ToString();
                    message.SendersNickname = $"{user2.FirstName} {user2.LastName}";
                    break;
                case DialogueType.Group:
                    if (message.Id == (vkApi.UserId ?? 0).ToString())
                        message.SendersNickname = nickname;

                    string id = message.Id.ToString();
                    message.SendersNickname = vkApi.Groups.GetById(new Collection<string>() { id },
                        id, VkNet.Enums.Filters.GroupsFields.Status)[0].Name;
                    message.SendersId = id;
                    break;
            }
        }

        public static long PeerIdToId(long peerId, DialogueType dialogueType)
        {
            switch (dialogueType)
            {
                case DialogueType.Private:
                    return peerId;
                case DialogueType.Conversation:
                    return peerId;
                case DialogueType.Group:
                    return peerId * -1;
            }
            return -1;
        }

        public static DialogueType DetermineDialogueTypeFromPeerId(long peerId)
        {
            if (peerId >= 2000000000)
            {
                return DialogueType.Conversation;
            }
            else if (peerId < 0)
            {
                return DialogueType.Group;
            }
            else
            {
                return DialogueType.Private;
            }
        }

        public static string GetNicknameFromApi(VkApi vkApi)
        {
            var response = vkApi.Users.Get(new Collection<long>() { vkApi.UserId ?? 0 });
            return $"{response[0].FirstName} {response[0].LastName}";
        }


        public static string FindNickname(Collection<User> profiles, Collection<Group> groups,
                                          MessageModel message)
        {
            switch (message.DialogueType)
            {
                case DialogueType.Conversation:
                case DialogueType.Private:
                    foreach (User user in profiles)
                    {
                        if (user.Id == long.Parse(message.SendersId))
                        {
                            return $"{user.FirstName} {user.LastName}";
                        }
                    }
                    break;
                case DialogueType.Group:
                    foreach (Group group in groups)
                    {
                        if (group.Id == long.Parse(message.DialogueId))
                        {
                            return group.Name;
                        }
                    }
                    break;
            }

            return null;
        }


        public static string ParseServiceMessage(Message message, string dialogueId, VKProvider provider)
        {
            VkApi vkApi = provider.VkApi;
            if (message.Action.Type == MessageAction.ChatInviteUser |
                message.Action.Type == MessageAction.ChatInviteUserByLink)
            {
                User user = GetUserById(message.Action.MemberId ?? 0, vkApi);
                string nickname = $"{user.FirstName} {user.LastName}";
                return string.Format(LocalizationManager.Instance.Localization.UserInvited, nickname);
            }
            else if (message.Action.Type == MessageAction.ChatKickUser)
            {
                User user = GetUserById(message.Action.MemberId ?? 0, vkApi);
                string nickname = $"{user.FirstName} {user.LastName}";
                return string.Format(LocalizationManager.Instance.Localization.UserKicked, nickname);
            }
            else if (message.Action.Type == MessageAction.ChatPhotoUpdate)
            {
                ConversationPhotoWasUpdatedEventArgs eventArgs = new();
                eventArgs.Provider = provider;
                if (message.Action.Photo is not null)
                {
                    eventArgs.Photo50 = message.Action.Photo.Photo50.ToString() ?? "";
                    eventArgs.Photo100 = message.Action.Photo.Photo100.ToString() ?? "";
                    eventArgs.Photo200 = message.Action.Photo.Photo200.ToString() ?? "";
                }
                OnConversationPhotoWasUpdated(eventArgs);
                return LocalizationManager.Instance.Localization.ConversationPhotoUpdated;
            }
            else if (message.Action.Type == MessageAction.ChatPhotoRemove)
            {
                return LocalizationManager.Instance.Localization.ConversationPhotoRemove;
            }
            else if (message.Action.Type == MessageAction.ChatPinMessage)
            {
                return LocalizationManager.Instance.Localization.MessagePinned;
            }
            else if (message.Action.Type == MessageAction.ChatTitleUpdate)
            {
                string title = message.Action.Text;
                OnConversationTitleWasUpdated(new()
                {
                    DialogueId = dialogueId,
                    NewTitle = title,
                    Provider = provider
                });
                return string.Format(LocalizationManager.Instance.Localization.ConversationTitleUpdated, title);
            }
            else if (message.Action.Type == MessageAction.ChatCreate)
            {
                return LocalizationManager.Instance.Localization.ConversationCreated;
            }

            return "";
        }

        public static ProfilesAndGroupsModel CreatePAG(Collection<User> profiles, Collection<Group> groups)
        {
            return new()
            {
                Users = profiles,
                Groups = groups
            };
        }

        public static ProfilesAndGroupsModel CreatePAG(ReadOnlyCollection<User> profiles,
                                                       ReadOnlyCollection<Group> groups)
        {
            return new()
            {
                Users = profiles.SafeToCollection(),
                Groups = groups.SafeToCollection()
            };
        }

        public static void AttachAvatar(Uri smallPhoto, Uri bigPhoto, DialogueShort dialogue)
        {
            string avatarUrl = smallPhoto.ToString();
            string fullAvatarUrl = bigPhoto is not null
                                    ? bigPhoto.ToString() : avatarUrl;

            if (avatarUrl != "")
            {
                dialogue.AvatarUrl = avatarUrl;
                dialogue.FullAvatarUrl = fullAvatarUrl;
            }
        }

        private static User GetUserById(string id, VkApi vkApi)
        {
            var ids = new Collection<long>() { long.Parse(id) };
            return vkApi.Users.Get(ids)[0];
        }

        private static User GetUserById(long id, VkApi vkApi)
        {
            var ids = new Collection<long>() { id };
            return vkApi.Users.Get(ids)[0];
        }


        protected static void OnConversationTitleWasUpdated(ConversationTitleWasUpdatedEventArgs eventArgs)
        {
            EventHandler<ConversationTitleWasUpdatedEventArgs> eventHandler = VKUConversationTitleWasUpdated;
            eventHandler?.Invoke(typeof(VKUtils), eventArgs);
        }

        protected static void OnConversationPhotoWasUpdated(ConversationPhotoWasUpdatedEventArgs eventArgs)
        {
            EventHandler<ConversationPhotoWasUpdatedEventArgs> eventHandler = VKUConversationPhotoWasUpdated;
            eventHandler?.Invoke(typeof(VKUtils), eventArgs);
        }
    }
}