﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MChat.BackEnd.DataModels;
using MChat.BackEnd.DataModels.Attachments;
using MChat.BackEnd.Providers;
using MChat.BackEnd.Templates;
using MChat.BackEnd.Utils.VK;
using VkNet;
using VkNet.Enums;
using VkNet.Model;
using VkNet.Model.Attachments;

namespace MChat.BackEnd.Utils
{
    public class VkFactory
    {

        private VKProvider _provider;

        public VkFactory(VKProvider provider)
        {
            _provider = provider;
        }

        public UserModel BuildUserObject(User user)
        {
            return new()
            {
                Nickname = $"{user.FirstName} {user.LastName}",
                Id = user.Id.ToString(),
                AvatarUrl = user.Photo50.ToString(),
                FullAvatarUrl = user.PhotoMaxOrig is not null
                                           ? user.PhotoMaxOrig.ToString() : user.Photo50.ToString()
            };
        }

        public DialogueShort CreateGroupDialogue(Conversation conversation,
                                                Message lastMessage,
                                                ReadOnlyCollection<User> profiles,
                                                ReadOnlyCollection<Group> groups)
        {
            ProfilesAndGroupsModel pag = VKUtils.CreatePAG(profiles, groups);

            DialogueShort dialogue = null;
            foreach (Group group in groups)
            {
                if (group.Id == conversation.Peer.Id * -1)
                {
                    dialogue = CreateDialogueShortBase(conversation, group.Name, 
                                                DialogueType.Group, lastMessage, pag);
                    VKUtils.AttachAvatar(group.Photo50, group.Photo200, dialogue);
                    break;
                }
            }


            return dialogue;
        }

        public DialogueShort CreatePrivateDialogue(Conversation conversation,
                                                    Message lastMessage,
                                                    ReadOnlyCollection<User> profiles,
                                                    ReadOnlyCollection<Group> groups)
        {
            ProfilesAndGroupsModel pag = VKUtils.CreatePAG(profiles, groups);
            DialogueShort dialogue = null;
            foreach (User user in profiles)
            {
                if (user.Id == conversation.Peer.Id)
                {
                    string title = $"{user.FirstName} {user.LastName}";

                    dialogue = CreateDialogueShortBase(conversation, title, DialogueType.Private, 
                                                       lastMessage, pag);
                    VKUtils.AttachAvatar(user.Photo50, user.PhotoMaxOrig, dialogue);

                    break;
                }
            }
            return dialogue;
        }

        public DialogueShort CreateConversationDialogue(Conversation conversation,
                                                        Message lastMessage,
                                                        ReadOnlyCollection<User> profiles,
                                                        ReadOnlyCollection<Group> groups)
        {
            ProfilesAndGroupsModel pag = VKUtils.CreatePAG(profiles, groups);

            var dialogue = CreateDialogueShortBase(conversation, conversation.ChatSettings.Title, 
                                                    DialogueType.Conversation, lastMessage, pag);
            if (conversation.ChatSettings.Photo is not null)
            {
                VKUtils.AttachAvatar(conversation.ChatSettings.Photo.Photo50, 
                                    conversation.ChatSettings.Photo.BigPhotoSrc, dialogue);
            }

            return dialogue;
        }

        public MessageModel CreateMessageModel(Message message)
        {
            MessageModel messageModel = new (message);

            if (message.Action is not null)
            {
                string serviceMessageText = VKUtils.ParseServiceMessage(message, messageModel.Id,
                                                                        _provider);
                if (serviceMessageText != "")
                {
                    messageModel.Body = serviceMessageText;
                    messageModel.Service = true;
                }
            }
            if (message.Attachments.Count > 0)
            {
                foreach (Attachment attach in message.Attachments)
                {
                    IAttachmentModel model = CreateAttachment(attach);
                    if (model is not null) messageModel.Attachments.Add(model);
                }
            }
            if (message.ForwardedMessages is not null)
            {
                foreach (Message fwdMessage in message.ForwardedMessages)
                    messageModel.FwdMessages.Add(CreateMessageModel(fwdMessage));
            }
            if (message.ReplyMessage is not null)
            {
                messageModel.FwdMessages.Add(CreateMessageModel(message.ReplyMessage));
            }

            return messageModel;
        }

        private DialogueShort CreateDialogueShortBase(Conversation conversation,
                                                      string title,
                                                      DialogueType dialogueType,
                                                      Message lastMessage,
                                                      ProfilesAndGroupsModel pag)
        {
            DialogueShort dialogue = new DialogueShort();

            dialogue.LastMessage = CreateMessageModel(lastMessage);

            dialogue.UnreadCount = conversation.UnreadCount ?? 0;
            dialogue.Title = title;
            dialogue.DialogueType = dialogueType;
            dialogue.Id = VKUtils.PeerIdToId(conversation.Peer.Id, dialogue.DialogueType).ToString();
            return dialogue;
        }

        private IAttachmentModel CreateAttachment(Attachment attachment)
        {
            if (attachment.Instance is Photo)
            {
                return CreatePhotoAttachmentModel(attachment.Instance as Photo);
            }
            else if (attachment.Instance is Audio)
            {
                return CreateAudioAttachmentModel(attachment.Instance as Audio);
            }
            else if (attachment.Instance is Video)
            {
                return CreateVideoAttachmentModel(attachment.Instance as Video);
            }
            else if (attachment.Instance is Document)
            {
                return CreateDocumentAttachmentModel(attachment.Instance as Document);
            }
            else if (attachment.Instance is Link)
            {
                return CreateLinkAttachmentModel(attachment.Instance as Link);
            }
            else if (attachment.Instance is AudioMessage)
            {
                return CreateVoiceMessageAttachmentModel(attachment.Instance as AudioMessage);
            }
            return null;
        }

        private VoiceMessageAttachmentModel CreateVoiceMessageAttachmentModel(AudioMessage audioMessage)
        {
            return new VoiceMessageAttachmentModel()
            {
                Url = audioMessage.LinkOgg.ToString(),
                OwnerId = audioMessage.OwnerId.ToString(),
                Duration = ((int)audioMessage.Duration),
                Waveform = audioMessage.Waveform.SafeToCollection(),
                Id = (audioMessage.Id ?? 0).ToString(),
                AccessKey = audioMessage.AccessKey,
            };
        }

        private PhotoAttachmentModel CreatePhotoAttachmentModel(Photo photo)
        {
            Collection<PhotoAttachmentModel> sizes = new Collection<PhotoAttachmentModel>();

            foreach (PhotoSize photoSize in photo.Sizes)
            {
                sizes.Add(new()
                {
                    Url = photoSize.Url.ToString(),
                    Width = photoSize.Width,
                    Height = photoSize.Height
                });
            }

            string ownerId = photo.UserId.ToString() ?? "";
            PhotoAttachmentModel biggestPhoto = sizes[sizes.Count - 1];

            return new PhotoAttachmentModel()
            {
                Width = biggestPhoto.Width,
                Height = biggestPhoto.Height,
                Url = biggestPhoto.Url,
                Sizes = sizes,
                OwnerId = ownerId
            };
        }

        private AudioAttachmentModel CreateAudioAttachmentModel(Audio audio)
        {
            return new AudioAttachmentModel()
            {
                Artist = audio.Artist,
                Title = audio.Title,
                Duration = audio.Duration,
                Id = audio.Id.ToString(),
                OwnerId = audio.OwnerId.ToString(),
                Url = audio.Url.ToString(),
                AccessKey = audio.AccessKey,
            };
        }

        private VideoAttachmentModel CreateVideoAttachmentModel(Video video)
        {
            Collection<VideoImage> videoImageCollection = video.Image.SafeToCollection();
            VideoImage videoImage = videoImageCollection[videoImageCollection.Count - 1];
            VideoThumbnailModel videoThumbnail = new VideoThumbnailModel()
            {
                Url = videoImage.Url.ToString(),
                Width = (int)videoImage.Width,
                Height = (int)videoImage.Height,
            };

            return new VideoAttachmentModel()
            {
                Title = video.Title,
                Description = video.Description,
                Thumbnail = videoThumbnail,
                Duration = video.Duration ?? 0,
                ViewsCount = video.Views ?? 0,
                Width = (int)video.Width,
                Height = (int)video.Height,
                Id = video.Id.ToString(),
                OwnerId = video.OwnerId.ToString(),
                Url = video.Player.ToString(),
                AccessKey = video.AccessKey
            };
        }

        private DocumentAttachmentModel CreateDocumentAttachmentModel(Document document)
        {
            return new DocumentAttachmentModel()
            {
                Title = document.Title,
                Size = document.Size ?? 0,
                Extension = document.Ext,
                Id = document.Id.ToString(),
                OwnerId = document.OwnerId.ToString(),
                AccessKey = document.AccessKey,
                Url = document.Uri.ToString()
            };
        }

        private LinkAttachmentModel CreateLinkAttachmentModel(Link link)
        {
            return new()
            {
                Caption = link.Caption,
                Description = link.Description,
                PreviewUrl = link.Image,
                Id = link.Id.ToString(),
                OwnerId = link.OwnerId.ToString(),
                Url = link.Uri.ToString(),
                AccessKey = link.AccessKey,
            };
        }


    }

}
