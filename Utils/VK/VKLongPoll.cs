using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;

using MChat.BackEnd.DataModels;
using MChat.BackEnd.Providers;
using MChat.BackEnd.Utils;

using Newtonsoft.Json.Linq;
using VkNet;
using VkNet.Model;
using VkNet.Utils;

namespace MChat.BackEnd.Utils.VK
{
    class VKLongPoll
    {
        public event EventHandler<NewMessageReceivedEventArgs> LPNewMessageReceived;
        public event EventHandler<UserIsTypingEventArgs> LPUserIsTyping;
        public event EventHandler<UsersAreTypingEventArgs> LPUsersAreTyping;
        public event EventHandler<UsersAreRecordingAVoiceMessageEventArgs> LPUsersAreRecordingAVoiceMessage;
        public bool Work { get; set; }
        private WebClient _webClient = new() { Encoding = Encoding.UTF8 };
        private string _nickname;
        private VkApi _api;
        private ulong _ts;
        private VKProvider _provider;

        public VKLongPoll(VKProvider provider, string nickname)
        {
            _nickname = nickname;
            _provider = provider;
            _api = _provider.VkApi;
        }

        public async void Update()
        {
            var server = _api.Messages.GetLongPollServer(true);
            Console.WriteLine(_api.Token);
            _ts = ulong.Parse(server.Ts);

            while (Work)
            {
                string url = $"https://{server.Server}?act=a_check&key={server.Key}&ts={_ts}&wait=25&version=2&mode=2";
                JToken result = await Network.RequestAsync(url);

                if (result["failed"] is not null)
                {
                    int errorCode = (int)result["failed"];
                    if (errorCode == 1)
                    {
                        server.Ts = result.SelectToken("$.ts").ToString();
                    }
                    else if (errorCode == 2 || errorCode == 3)
                    {
                        server = _api.Messages.GetLongPollServer(true);
                    }
                    else
                    {
                        return;
                    }
                    continue;
                }
                _ts = (ulong)result["ts"];
                ProcessUpdate(result);
            }
        }

        public Collection<MessageModel> GetHistory(ulong ts)
        {
            Collection<MessageModel> messages = new ();
            var response = _provider.VkApi.Messages.GetLongPollHistory(new() { Ts = ts });
            foreach (Message message in response.Messages)
                messages.Add(_provider.Factory.CreateMessageModel(message));
            
            return messages;
        }

        private void ProcessUpdate(JToken result)
        {
            foreach (JToken update in result["updates"])
            {
                switch ((int)update[0])
                {
                    case 4:
                        {
                            MessageModel messageObj = VKUtils.ProcessLongPollMessage(update, _provider);
                            OnNewMessageReceived(new() { Message = messageObj, Provider = _provider });
                            break;
                        }
                    case 61:
                        OnUserIsTyping(new()
                        {
                            UserId = (ulong)update[1],
                            DialogueType = DialogueType.Private,
                            Provider = _provider
                        });
                        break;
                    case 62:
                        OnUserIsTyping(new()
                        {
                            UserId = (ulong)update[1],
                            DialogueType = DialogueType.Private,
                            DialogueId = (long)update[2],
                            Provider = _provider
                        });
                        break;
                    case 63:
                        {
                            long peerId = (long)update[2];
                            DialogueType dialogueType = VKUtils.DetermineDialogueTypeFromPeerId(peerId);
                            OnUsersIsTyping(new()
                            {
                                UsersId = update[1].ToObject<Collection<ulong>>(),
                                DialogueType = dialogueType,
                                DialogueId = VKUtils.PeerIdToId(peerId, dialogueType),
                                PeerId = peerId,
                                TotalCount = (int)update[3],
                                Provider = _provider
                            });

                            _ts = (ulong)update[4];
                            break;
                        }
                    case 64:
                        {   
                            long peerId = (long)update[1];
                            DialogueType dialogueType = VKUtils.DetermineDialogueTypeFromPeerId(peerId);
                            OnUsersAreRecordingAnVoiceMessage(new()
                            {
                                UsersId = update[2].ToObject<Collection<ulong>>(),
                                DialogueType = dialogueType,
                                DialogueId = VKUtils.PeerIdToId(peerId, dialogueType),
                                PeerId = peerId,
                                TotalCount = (int)update[3],
                                Provider=_provider
                            });

                            _ts = (ulong)update[4];
                            break;
                        }
                }
            }
        }

        protected virtual void OnNewMessageReceived(NewMessageReceivedEventArgs eventArgs)
        {
            EventHandler<NewMessageReceivedEventArgs> newMessageEventHandler = LPNewMessageReceived;
            newMessageEventHandler?.Invoke(this, eventArgs);
        }

        protected virtual void OnUserIsTyping(UserIsTypingEventArgs eventArgs)
        {
            EventHandler<UserIsTypingEventArgs> userIsTypingEventHandler = LPUserIsTyping;
            userIsTypingEventHandler?.Invoke(this, eventArgs);
        }

        protected virtual void OnUsersIsTyping(UsersAreTypingEventArgs eventArgs)
        {
            EventHandler<UsersAreTypingEventArgs> usersIsTypingEventHandler = LPUsersAreTyping;
            usersIsTypingEventHandler?.Invoke(this, eventArgs);
        }

        protected virtual void OnUsersAreRecordingAnVoiceMessage(UsersAreRecordingAVoiceMessageEventArgs eventArgs)
        {
            EventHandler<UsersAreRecordingAVoiceMessageEventArgs> eventHandler = LPUsersAreRecordingAVoiceMessage;
            eventHandler?.Invoke(this, eventArgs);
        }
    }
}