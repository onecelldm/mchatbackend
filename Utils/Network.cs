using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MChat.BackEnd.Utils
{
    public class Network
    {
        public static async Task<JObject> RequestAsync(string url)
        {
            var res = GlobalInstances.HttpClient.GetAsync(url).Result;

            string awaitData = await res.Content.ReadAsStringAsync();
            return JObject.Parse(awaitData);
        }
    }
}