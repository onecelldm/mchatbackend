using System;
using System.Net.Http;

namespace MChat.BackEnd.Utils
{
    class GlobalInstances
    {
        public static HttpClient HttpClient = new HttpClient();
        public static Random Random = new Random();
    }
}