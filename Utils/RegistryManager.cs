using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using MChat.BackEnd.DataModels;
using MChat.BackEnd.Templates;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MChat.BackEnd.Utils
{
    public class ConfigManager
    {
        [SupportedOSPlatform("windows")]
        public static Collection<AccountModel> ReadRegistry()
        {
            Collection<AccountModel> result = new Collection<AccountModel>();
            RegistryKey key = null;
            try
            {
                key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true)?.CreateSubKey(Settings.AppName);
                if (key is not null)
                {
                    string data = (string)key.GetValue(Settings.AccountsRegistryName);
                    if (data is not null)
                    {
                        result = JsonConvert.DeserializeObject<Collection<AccountModel>>(data);
                    }
                }
            }
            finally
            {
                key?.Close();
            }

            return result;
        }

        [SupportedOSPlatform("windows")]
        public static void SaveRegistry(Collection<AccountModel> accounts)
        {
            RegistryKey key = null;
            string serializedAccounts = JsonConvert.SerializeObject(accounts);
            try
            {
                key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true)?.CreateSubKey(Settings.AppName);
                key?.SetValue(Settings.AccountsRegistryName, serializedAccounts);
            }
            finally
            {
                key?.Close();
            }
        }

        public static Collection<AccountModel> ReadConfig()
        {
            Collection<AccountModel> accounts = new Collection<AccountModel>();
            if (File.Exists(Settings.AccountsFilePath))
            {
                var accountList = JArray.Parse(File.ReadAllText(Settings.AccountsFilePath));
                foreach (var accountRaw in accountList)
                {
                    accounts.Add(accountRaw.ToObject<AccountModel>());
                }
            }
            return accounts;
        }

        public static void SaveConfig(Collection<AccountModel> accounts)
            => File.WriteAllText(Settings.AccountsFilePath, JsonConvert.SerializeObject(accounts));

        public static Collection<AccountModel> Read()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return ReadRegistry();
            }
            else
            {
                return ReadConfig();
            }
        }

        public static void Save(Collection<AccountModel> accounts)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                SaveRegistry(accounts);
            }
            else
            {
                SaveConfig(accounts);
            }
        }
    }
}