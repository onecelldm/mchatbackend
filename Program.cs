﻿using System;
using System.IO;
using MChat.BackEnd.DataModels;
using MChat.BackEnd.Templates;
using MChat.BackEnd.Utils;

namespace Mchat.BackEnd
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkWithLongpoll();
            // RegisterAnAccount();
        }

        public static void WorkWithLongpoll()
        {
            IProvider provider = ProviderManager.Instance.ProviderList[0];
            provider.StartListener();
        }

        public static void WorkWithDialogue()
        {
            IProvider provider = ProviderManager.Instance.ProviderList[0];
            var dialogueList = provider.GetDialogueList();
            DialogueShort dialogue = dialogueList[0];

            var dialogueHistory = provider.GetDialogueMessageHistory(dialogue.Id);
            foreach (MessageModel message in dialogueHistory) Console.WriteLine($"{message.Service} {message.Body}");
        }

        public static void RegisterAnAccount()
        {
            string[] credentials = File.ReadAllLines("passwords");
            ProviderManager.Instance.RegisterAnAccount(credentials[0], credentials[1], "whtntr.vkprovider");
        }
    }
}
