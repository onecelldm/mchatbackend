using MChat.BackEnd.Localization;

namespace MChat.BackEnd
{
    public class Settings
    {
        public static string AccountsRegistryName { get; private set; } = "Accounts";
        public static string AccountsFilePath { get; private set; } = "accdatapath.json";
        public static ILocalization Localization { get; private set; } = new RuRuLocalization();
        public static string AppName { get; private set; } = "MChat";
    }
}