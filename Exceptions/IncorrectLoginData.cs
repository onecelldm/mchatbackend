using System;

namespace MChat.BackEnd.Exceptions
{
    public class IncorrectLoginData : Exception
    {
        public IncorrectLoginData() : base() { }
    }
}