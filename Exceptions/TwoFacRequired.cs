using System;

namespace MChat.BackEnd.Exceptions
{
    public class TwoFacRequired : Exception
    {
        public TwoFacRequired() : base() { }
    }
}