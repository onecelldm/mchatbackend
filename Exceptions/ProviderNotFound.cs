
using System;

namespace MChat.BackEnd.Exceptions
{
    public class ProviderNotFoundException : Exception
    {
        public ProviderNotFoundException() : base() { }
    }
}