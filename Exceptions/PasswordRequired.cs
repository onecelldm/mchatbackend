
using System;

namespace MChat.BackEnd.Exceptions
{
    public class PasswordRequired : Exception
    {
        public PasswordRequired() : base() { }
    }
}