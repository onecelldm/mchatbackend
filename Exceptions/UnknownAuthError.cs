using System;

namespace MChat.BackEnd.Exceptions
{
    public class UnknownAuthError : Exception
    {
        public UnknownAuthError() : base() { }
        public UnknownAuthError(string message) : base(message) { }
    }
}